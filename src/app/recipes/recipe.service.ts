import { Recipe } from './recipe.model';
import { EventEmitter } from '@angular/core';

export class RecipeService{

    recipeSelected = new EventEmitter<Recipe>();
    private recipes: Recipe[] = [
        new Recipe('A test recipe', 'A test', 'http://appsgeyser.com/blog/wp-content/uploads/2016/12/recipe-icon-6.png'),
        new Recipe('A new recipe', 'Manso', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/chicken-fricassee-horizontal-1536771733.png')
    ];

    getRecipes(){
        return this.recipes.slice();
    }
}